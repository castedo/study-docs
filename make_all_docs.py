#!/usr/bin/python3

import jinjagen, jankyjats
from baseprinter.out import make_jats_xml
from contextlib import chdir

# std lib
from pathlib import Path


gen = jinjagen.JinjaGeneratorCore("src")
gen.hook_module("jinjagenadd")

for entry in gen.source.iterdir():
    if entry.is_dir():
        docid = entry.name
        dest = Path("doc") / docid
        pandocin = entry / "pandocin.yaml"
        if pandocin.exists():
            if not dest.exists():
                dest = dest.absolute()
                with chdir(entry):
                    make_jats_xml(dest, [], ["pandocin.yaml"])
        else:
            jankyjats.make_jats_with_notebooks(
                Path("doc") / docid,
                gen.source / docid / "jats.md.jinja",
                Path("_build") / docid,
                gen,
                dict(docid=docid),
            )
