all:
	./make_all_docs.py
	@echo Done

clean:
	rm -rf _build/
	rm -r doc/*/article.xml

.PHONY : all clean
