from epijats.util import up_to_date

# standard library
import os, sys, shutil, subprocess

def make_jats_xml(target, source, temp, gen, ctx=dict()):
    if up_to_date(target, source):
        return
    temp_md = temp / "pack.md"
    os.makedirs(temp_md.parent, exist_ok=True)
    tmpl_path = source.relative_to(gen.source)
    ctx = dict(ctx, this=dict(dirpath=tmpl_path.parent))
    gen.render_file(tmpl_path, temp_md, ctx)

    meta = "--metadata-file {}".format(gen.source / "common-meta.yaml")
    cmd = "./pandoc-jats.sh {} {} {}".format(target, meta, temp_md)
    print(cmd)
    subprocess.run(cmd, shell=True, check=True, stdout=sys.stdout, stderr=sys.stderr)


def make_jats_dir(target, source, temp, gen, ctx=dict()):
    os.makedirs(target, exist_ok=True)
    xml_file = target / "article.xml"
    make_jats_xml(xml_file, source, temp, gen, ctx)

    pass_from = source.parent / "pass"
    pass_to = target / "pass"
    if pass_from.exists() and not pass_to.exists():
        assert pass_from.is_dir()
        shutil.copytree(pass_from, pass_to)


def make_jats_with_notebooks(target, source, temp, gen, ctx=dict()):
    make_jats_dir(target, source, temp, gen, ctx)
    for fp in source.parent.iterdir():
        if fp.name.endswith(".ipynb"):
            exec_notebook(temp / "nb" / fp.name, fp)
            pass_dir = target / "pass"
            if not pass_dir.exists():
                shutil.copytree(temp / "nb", pass_dir)


def exec_notebook(target, source):
    assert source.name.endswith(".ipynb"), "Not a notebook: {}".format(source)
    if up_to_date(target, source):
        return
    os.makedirs(target.parent, exist_ok=True)
    shutil.copy(source, target)
    cmd = "jupyter nbconvert --execute --to=html {}".format(target)
    print(cmd)
    subprocess.run(cmd, shell=True, check=True, stdout=sys.stdout, stderr=sys.stderr)
