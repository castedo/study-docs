#!/bin/bash

DEST=$1
shift

pandoc \
  --data-dir config_jats \
  --defaults jats.yaml \
  $@ \
  > $DEST
