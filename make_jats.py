#!/usr/bin/python3

import jinjagen, jankyjats

# std lib
import argparse
from pathlib import Path

parser = argparse.ArgumentParser(description="make JATS")
parser.add_argument('docid', help="docid")
parser.add_argument('source', help="source")
parser.add_argument('target', help="target")
args = parser.parse_args()

gen = jinjagen.JinjaGeneratorCore(args.source)
gen.hook_module("jinjagenadd")

jankyjats.make_jats_with_notebooks(
    Path(args.target) / args.docid,
    gen.source / args.docid / "jats.md.jinja",
    Path("_build") / args.docid,
    gen,
)
