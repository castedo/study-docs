{% extends '_doc.md.jinja' %}
{% set docid = 136 %}
{% set title = "A formal definition of F~ST~" %}
{% set last_update = '2021-12-07' %}

{% block doc_content %}
{{shared_latex()}}

**NOTE:** A newly published article on $F_{ST}$ [@ochoa_estimating_2021] looks like
a promising source of a better formal definition.

The name $F_{ST}$ was used as early as [@wright_genetical_1949] for a certain
measure between populations. In the many decades since, the name has been
used to have slightly different meanings. The recent publication
[@bhatia_estimating_2013] covers a long list of different $F_{ST}$ meanings in
many papers over the decades.
Most of the papers cover additional concepts which are not required to define
and gain intuition on $F_{ST}$. And the mathematical details behind a formal
definition are spread across many papers.

This documents gives a simple formal definition to $F_{ST}$, equivalent to
[@bhatia_estimating_2013] and [@patterson_notes_nodate]. This simple definition
also makes clear how $F_{ST}$ is precisely a ratio of variances.

## The Definition

Random variables $A_S$, $A_T$ and $D$ model uncertainty for $F_{ST}$:

* $A_S$ and $A_T$ for the allele found in a random gamete
 from the "**S**ub-population" and "**T**op" population, respectively
* $D$ for the random **d**ecent, **d**rift, or **d**ivergence
 of the "sub-population" from the "top" population

"Top" population can mean ancestral population (as in
[@bhatia_estimating_2013]), or it can mean "total" population
(the original meaning in [@wright_genetical_1949]).

Given assumptions

* $A_S$ and $A_T$ take values of $0$ or $1$
* $A_T$ and $D$ are independent
* $\E{A_S} = \E{A_T}$

the definition follows

$$
F_{ST} := \frac{ \Var{\E{A_S|D}} }{ \Var{A_T} }
$$

## Convenient Expectations

Conveniently, expectations of allele variables are allele frequencies.
The following variable definition will be convenient
$$
p := \E{A_T}
$$

Due to the assumptions of $F_{ST}$ the following are conveniently true
$$
\begin{aligned}
p &= \E{A_S}  &  p &= \E{A_T^2}  \\
\E{A_S} &= \E{A_S^2}  &  \E{A_S|D} &= \E{A_S^2|D}  \\
\end{aligned}
$$
and
$$
\Var{A_T} = \E{A_T^2} - \E{A_T}^2 = p - p^2  =  p(1-p)
$$



## $F_{ST}$ as variance explained or uncertainty reduced

In light of the following theorem, $F_{ST}$ can be interpreted as allele
variance explained by random descent/drift/divergence.  Alternatively, an
interpretation can also be allele uncertainty reduced by knowing
descent/drift/divergence.

**Theorem 1**

$$
\Var{A_T} = \Var{\E{A_S|D}} + \E{\Var{A_S|D}}
$$

**Proof**

$$
\begin{aligned}
\Var{\E{A_S|D}}
  & = \E{\E{A_S|D}^2} - \E{\E{A_S|D}}^2   \\
  & = \E{\E{A_S|D}^2} - p^2   \\
\E{\Var{A_S|D}}
  & = \E{ \E{A_S^2|D} - \E{A_S|D}^2 }  \\
  & = p - \E{\E{A_S|D}^2}  \\
\end{aligned}
$$


## Unbiased Estimators

Consider observing two independent random descents/drifts/divergences $D_1$
and $D_2$ under the assumptions for $D$ of $F_{ST}$.
Furthermore, for each $j \in \{1, 2 \}$,
consider observing $n_j$ independent random gametes within each resulting
sub-population. Define $n_1 + n_2$ independent observed alleles $A_{S,j,i}$
with $i$ indexing sampled gametes within each sampled sub-populations resulting
from the independent descents/drifts/divergences. For convenience define the
following:
$$
\begin{aligned}
\hat{p}_1 & := \frac{1}{n_1} \sum_{i=1}^{n_1} A_{S,1,i}  &
\hat{p}_2 & := \frac{1}{n_2} \sum_{i=1}^{n_2} A_{S,2,i}
\end{aligned}
$$

The "Hudson" estimator of $F_{ST}$ is defined in [@bhatia_estimating_2013]
as 
$$
\frac{
  (\hat{p}_1 - \hat{p}_2)^2
   - \frac{\hat{p}_1 (1 - \hat{p}_1)}{n_1-1}
   - \frac{\hat{p}_2 (1 - \hat{p}_2)}{n_2-1}
}{
  \hat{p}_1 (1-\hat{p}_2) + \hat{p}_2 (1-\hat{p}_1)
}
$$

We first show that the denominator is an unbiased estimator of
$2 \Var{A_T}$.
With $\hat{p}_1$ and $\hat{p}_1$ independent it follows:

$$
\begin{aligned}
\E{ \hat{p}_1 (1-\hat{p}_2) + \hat{p}_2 (1-\hat{p}_1) }
  & = \E{\hat{p}_1} \E{1-\hat{p}_2} + \E{\hat{p}_2} \E{1-\hat{p}_1}  \\
  & = 2 p (1-p)  \\
  & = 2 \Var{A_T}
\end{aligned}
$$

We now show the "Hudson" numerator is an unbiased estimator of
$2 \Var{\E{A_S|D}}$. For each $j \in \{0,1\}$ define:
$$
\hat{v}_j
  := \frac{1}{n_j-1} \sum_{i=1}^{n_j} (A_{S,j,i} - \hat{p}_i)^2
$$
It follows as a classic unbiased estimator of variance
[@degroot_probability_2002] that:
$$
\E{ \hat{v}_j } = \E{\Var{A_S|D_1}}
$$
Since $\E{\Var{A_S|D}} = \Var{A_T} - \E{\Var{A_S|D}}$, it follows
that an unbiased estimator of $2 \Var{\E{A_S|D}}$ is
$$
  \hat{p}_1 (1-\hat{p}_2) + \hat{p}_2 (1-\hat{p}_1) - \hat{v}_1 - \hat{v}_2 
$$
We now show this is equivalent to the "Hudson" numerator.
Note that
$$
\begin{aligned}
\hat{p}_1 (1-\hat{p}_2) + \hat{p}_2 (1-\hat{p}_1)
  & = \hat{p}_1 + \hat{p}_2 - 2 \hat{p}_1 \hat{p}_2  \\
\hat{v}_i & = \hat{p}_i - \hat{p}_i^2 + \frac{\hat{p}_i (1 - \hat{p}_i)}{n_i-1}  \\
(\hat{p}_1 - \hat{p}_2)^2 & = \hat{p}_1^2 + \hat{p}_2^2 - 2 \hat{p}_1 \hat{p}_2  \\
\end{aligned}
$$
it follows that
$$
\hat{p}_1 (1-\hat{p}_2) + \hat{p}_2 (1-\hat{p}_1) - \hat{v}_1 - \hat{v}_2
  = (\hat{p}_1 - \hat{p}_2)^2 - \frac{\hat{p}_1 (1 - \hat{p}_1)}{n_1-1} - \frac{\hat{p}_2 (1 - \hat{p}_2)}{n_2-1}
$$
which is the numerator in the "Hudson" estimator in [@bhatia_estimating_2013].

{% endblock doc_content %}

