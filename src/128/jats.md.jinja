{% extends '_doc.md.jinja' %}
{% set docid = 128 %}
{% set title = "Scoped Entropy" %}
{% set last_update = '2022-03-28' %}

{% block abstract %}
This WORKING DRAFT defines _scoped entropy_.
{% endblock abstract %}

{% block doc_content %}
{{shared_latex()}}

{% raw %}
\newcommand{\sps}[2]{\operatorname{dom}_{ #2}{ #1}}
\newcommand{\B}[2]{{B}_{ #1}(#2)}
\newcommand{\0}{\mathtt{0}}
\newcommand{\1}{\mathtt{1}}
\newcommand{\W}[2]{\operatorname{V}_{ #1}({ #2})}
\newcommand{\U}[2]{\operatorname{U}_{ #1}(#2)}
{% endraw %}

>
> ## WORKING DRAFT
>

Both variance and entropy are measures of uncertainty and thus information.
_Scoped entropy_ is a generalization of both statistical variance and Shannon
entropy [@shannon_mathematical_1998]. It jointly generalizes by using a
construct of _scope of relevance_.
_Scoped_ entropy can be appiled to both discrete and continuous variables.

A _random object_ is a function with a domain of a probability space
[@ash_probability_2000].  When the function values are real numbers, it is a
_random variable_ (also called _real random object_ in this document).
In this document, a _finite random object_ means a random object that takes on
finitely many values. Or in other words, the range of a _finite random object_
is a set of finite size.

Both entropy and variance are functions of random objects.
In the case of Shannon entropy, the random object is finite (with values often
referred to as symbols).
In the case of variance, the random object is a (real) random variable.
The distances between values of a finite random variable affect variance, but
not Shannon entropy.

Given a scope of relevance $\theta$ and a finite or real random object $X$,
scoped entropy is denoted as the function
$$
 \W{\theta}{X}
$$

There is a _unirelevant decomposition_ function $\mathfrak{h}$ which given any
finite random object $X$ generates a scope $\mathfrak{h}(X)$ such that
$$
\W{\mathfrak{h}(X)}{X} = \H{X}
$$
where $\H{X}$ is Shannon entropy.

Similarly, there is a _distribution decomposition_ function $\mathfrak{d}$
which given any random variable $X$ generates a scope $\mathfrak{d}(X)$ such that
$$
\W{\mathfrak{d}(X)}{X} = \Var{X}
$$


## Entropy Scope of Relevance

Let $\dom{f}$ denote the domain of a function $f$.
Given $\pi$ a set of sets, let $\selfcup{\pi}$ denote the union of all member
sets in $\pi$. When $\pi$ is a partition, $\pi$ covers $\selfcup{\pi}$.

In a _scope of relevance_, partitions represent questions with each member
set (part) an answer. Each answer represents an event of a probability space.

Given a function over partitions of events of probability space $\Omega$,
define levels of the domain:
$$
\begin{aligned}
\sps{\theta}{0} & :=
  \{ \pi \in \dom{\theta} : \selfcup{\pi} = \Omega \}  \\
\sps{\theta}{i+1} & := \left\{ \pi \in \dom{\theta} :
  \selfcup{\pi} \in \rho, \rho \in \sps{\theta}{i}
  , \pi \not\in \sps{\theta}{i}  \right\}  \\
\end{aligned}
$$

A _scope of relevance_ $\theta$ is a non-negative real-valued function
over partitions of events (subsets) of a probability space $\Omega$
with the following conditions:

1. At most one partition can cover any event.
   Formally, $\selfcup{\pi} = \selfcup{\rho}$ implies $\pi = \rho$
   for any partitions $\pi$ and $\rho$ in $\dom{\theta}$.
2. $\dom{\theta} = \bigcup_{i=0}^\infty \sps{\theta}{i}$
{# 3. Every partition has finitely many member sets (parts). #}

The domain of a scope of relevance is all relevant questions.
Each real value assigned to a question is a degree of relevance.
The second condition in the definition means the partitions (questions)
are dividing the probability space $\Omega$ in a nested hierarchical manner.
Or in other words, either the event of all outcomes ($\Omega$) or the event of
an answer to a question is the event under which another question can be asked.


## Scope product

$$
\begin{aligned}
\pi \times \rho & := \{ A \cap B : A \in \pi, B \in \rho \}  \\
\pi \times B & := \{ A \cap B : A \in \pi \}  \\
\end{aligned}
$$

Consider any two scopes $\theta$ and $\phi$ sharing probability space $\Omega$.
Define
$$
\theta \times \phi :=
  \bigcup_{i=0}^\infty
    \bigcup_{\substack{\pi \in \sps{\theta}{i} \\
                       \rho \in \sps{\phi}{i} \\
                       (\selfcup{\pi}) \cap (\selfcup{\rho}) \not= \emptyset }}
      \mathcal{C}(\pi, \rho)
$$
where
$$
\mathcal{C}(\pi, \rho) = \begin{cases}
  \{ (\pi \times \rho) \mapsto \theta(\pi) \}
    & \text{ when } \theta(\pi) = \phi(\rho)  \\
  \left\{ \pi \mapsto \theta(\pi) \right\} \cup
    \bigcup_{A \in \pi} \left\{ \rho \times A \mapsto \phi(\rho) \right\}
    & \text{ when } \theta(\pi) > \phi(\rho)  \\
  \left\{ \rho \mapsto \phi(\rho) \right\} \cup
    \bigcup_{A \in \rho} \left\{ \pi \times A \mapsto \theta(\pi) \right\}
    & \text{ when } \theta(\pi) < \phi(\rho)  \\
  \end{cases}
$$

#### Theorem (Scope product is a scope of relevance)

For any two scopes of relevance $\theta$ and $\phi$ sharing probability space
$\Omega$, $\theta \times \phi$ is also a scope of relevance.

**Proof**

TBD


## Distribution Decomposition

Given a set of random variables $\{ Z_i \}_{i \in I}$ with countable index set $I$,
and probability space $\Omega$,
for each $i \in I$,
define functions $\B{i}{s}$ and $c_i(s)$ over strings of alphabet
$\{ \0, \1 \}$ [@hopcroft_introduction_1979].

$$
\B{i}{\epsilon} := \Omega
$$
For any string $s$ with ${ \P{ \B{i}{s} } > 0}$ ,
$$
\begin{aligned}
c_i(s) & := \E{ Z_i | \B{i}{s} }  \\
\B{i}{s \0} & := \B{i}{s} \cap \{ Z_i < c_i(s) \} \\
\B{i}{s \1} & := \B{i}{s} \cap \{ Z_i > c_i(s) \} \\
\end{aligned}
$$
otherwise
$$
\B{i}{s\ell} := \emptyset
$$

A new probability space $\Omega'$ is defined by extending $\Omega$
such that for each $i \in I$ and string $s \in \{ \0, \1 \}^*$
with $\P{\B{i}{s}} - \P{ \{Z_i = c_i(s)\} } > 0$,
there are new disjoint events $C_{i,0}(s)$ and $C_{i,1}(s)$ such that
$$
\{Z_i = c_i(s)\} = C_{i,0}(s) \cup C_{i,1}(s)
$$
and for each $\ell \in \{ \0, \1 \}$
and all events $S$ in $\Omega$
$$
\P{ S \cap C_{i,\ell}(s) } = \P{ S \cap \{Z_i = c_i(s)\} }
    \frac{ \P{\B{i}{s\ell}} }{ \P{\B{i}{s}}-\P{ \{Z_i = c_i(s)\} } }
$$
Define
$$
\begin{aligned}
B'_i(s) & := \B{i}{s} \cup C_{i,\ell}(s) \\
v_i(s) & = \sum_{\ell \in \{ \0, \1 \}}
             \P{\B{i}{s\ell}|\B{i}{s}} \left( c_i(s\ell)-c(0) \right)^2  \\
h_i(s) & = \sum_{\ell \in \{ \0, \1 \}}
           h(\B{i}{s\ell}|\B{i}{s})  \\
\theta_i & :=
 \left\{ \{B'_i(s\0),B'_i(s\1)\} \mapsto v_i(s)/h_i(s)
         : \P{\B{i}{s}} > 0 \right\}  \\
\end{aligned}
$$

The scope product across all $\theta_i$ is the distribution decomposition
of $\{ Z_i \}_{i \in I}$.

>
> TODO: scope product across countable index set
>


## Scoped Entropy

Given a scope of relevance $\theta$, let:

$$
\begin{aligned}
\mathcal{A}_\theta(0)  & := \{ \Omega \}  \\
\mathcal{A}_\theta(i+1) & := \left\{
  A \cap B : A \in \mathcal{A}_\theta(i),
             B \in \pi, \pi \in \sps{\theta}{i} \right\}  \\
h(A|B) & := \P{A|B} \lb\frac{1}{\P{A|B}}  \\
\U{\pi}{Q} & :=
  \P{\selfcup{\pi}|Q} \sum_{S \in \pi}  h(S|\selfcup{\pi} \cap Q)  \\
\U{\theta}{Q} & :=
  \sum^\infty_{i = 0}
    \sum_{A \in \mathcal{A}_\theta(i)} \P{A|Q}
      \sum_{\pi \in \sps{\theta}{i}} \theta(\pi) \U{\pi}{A \cap Q} \\
\U{\theta}{\pi} & :=
  \sup \left\{
    \sum_{Q \in \rho} \P{Q} \U{\theta}{Q}
    : \text{ finite partition } \rho
           \underset{\text{(or equal to)}}{\text{ coarser than }} \pi
  \right\}  \\
\W{\theta}{Q} & := \U{\theta}{\Omega} - \U{\theta}{Q}  \\
\W{\theta}{\pi} & := \U{\theta}{\{\Omega\}} - \U{\theta}{\pi}  \\
\ker{X} & := \left\{
  \{ \omega \in \Omega : X(\omega)=v \} : v \text{ in the range of } X
 \right\} \\
\U{\theta}{X} & := \U{\theta}{\ker{X}}  \\
\W{\theta}{X} & := \W{\theta}{\ker{X}}  \\
\end{aligned}
$$


## Shannon Entropy Equality

Given any finite random object $X$, a _unirelevant decomposition_ is the
trivial mapping of $X$ to a scope $\theta$ assigning $1$ to a single partition
consisting of the events for each value taken by $X$:
$$
\begin{aligned}
\dom{\theta} & = \{\ker{X}\}  \\
\theta(\ker{X}) & = 1  \\
\end{aligned}
$$

### Theorem: Unirelevant Scoped Entropy equals Shannon Entropy

Given any finite random objects $X$ and $Y$ and scope $\theta$
equal to the _unirelevant decomposition_ of $X$,
$$
\U{\theta}{Y} = \H{X|Y}
$$
It follows as a corollary that
$$
\W{\theta}{X} = \H{X}
$$

**Proof**

Consider any single partition scope $\theta = \{\pi \mapsto 1 \}$.
Both $\mathcal{A}_\theta(i)$ and $\sps{\theta}{i}$ are only non-empty
at $i=0$ and equal $\{\Omega\}$ and $\{\pi\}$ respectively. Thus by definition
$$
\begin{aligned}
\U{\theta}{Y} & =
  \sum_{Q \in \ker{Y}} \P{Q}
  \P{\Omega|Q} \theta(\pi) \P{\selfcup{\pi}|\Omega,Q}
      \sum_{B \in \pi}  h(B|\selfcup{\pi},\Omega,Q)  \\
& = \sum_{Q \in \ker{Y}} \P{Q} \sum_{B \in \pi}  h(B|Q)  \\
& = \sum_{Q \in \ker{Y}} \sum_{B \in \pi} \P{Q \cap B} \lb\frac{1}{\P{B|Q}}  \\
& = \H{X|Q}  \\
\end{aligned}
$$
Proof of the corollary is:
$$
 \W{\theta}{X} = \U{\theta}{\Omega} - \U{\theta}{X}
               = \H{X|\{\Omega\}} - \H{X|X} = \H{X}
$$


## Benefit of scoped entropy over variance

A feature analogous to mutual information is the following:

For any random variable $X$ and random object $Y$ of finitely many values,
$$
\begin{array}{lcr}
\W{\mathfrak{d}(X)}{Y} = 0 & \text{ if and only if } & \text{ $X$ and $Y$ are independent}
\end{array}
$$

{% endblock doc_content %}

